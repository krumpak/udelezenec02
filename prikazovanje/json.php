<?php

function get_starost( $datum_rojstva ) {

	$starost = date( 'Y', strtotime( $datum_rojstva ) ) - date( 'Y' );

	$absolutna_starost = abs( $starost );

	$absolutna_starost = $absolutna_starost . ' let';

	return $absolutna_starost;
}

function get_dan( $dan_v_tednu, $zapis = 'S' ) {

	if ( strtolower( $zapis ) == 'l' ) {
		$sdnevi_v_tednu = array( 'Ponedeljek', 'Torek', 'Sreda', 'Četrtek', 'Petek', 'Sobota', 'Nedelja' );
	} else if ( strtolower( $zapis ) == 'm' ) {
		$sdnevi_v_tednu = array( 'Pon', 'Tor', 'Sre', 'Čet', 'Pet', 'Sob', 'Ned' );
	} else {
		$sdnevi_v_tednu = array( 'P', 'T', 'S', 'Č', 'P', 'S', 'N' );
	}

	return $sdnevi_v_tednu[ -- $dan_v_tednu ];
}

$url = 'http://students.b2.eu/api/osebe/';

if ( isset( $_GET['id'] ) ) {

	$url = 'http://students.b2.eu/api/osebe/' . $_GET['id'] . '/';
}
//$url  = 'http://students.b2.eu/api/osebe/5';
//$url  = 'http://students.b2.eu/api/osebe/random/10';
$json = file_get_contents( $url );

$podatki = json_decode( $json, true );
?>

<!doctype html>
<html lang="sl">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	<link rel="stylesheet" href="http://simplelineicons.com/css/simple-line-icons.css">
	<style>
		html, body {
			margin: 0;
			padding: 0;
		}

		table, th, td {
			border: 1px solid #ccc;
			border-collapse: collapse;
			transition: all .5s ease;
			margin: 0 auto;
		}

		th, td {
			padding: 5px 10px;
			font-size: 1rem;
		}

		th {
			background-color: #333;
			color: #fff;
		}

		tr:nth-child(odd) {
			background-color: #e0e0e0;
		}

		tr:hover {
			background-color: #faf4b2;
			cursor: pointer;
		}

		th a {
			color: white;
			text-decoration: none;
		}

		th a:hover {
			color: darkgray;
			text-decoration: underline;
		}

		td a {
			color: grey;
			text-decoration: none;
		}

		td a:hover {
			color: red;
			text-decoration: underline;
		}
	</style>
</head>
<body>

<table>
	<tr>
		<th>#</th>
		<th>Ime</th>
		<th>Priimek</th>
		<th>Izobrazba</th>
		<th>Naslov</th>
		<th>Kontakt</th>
		<th>Fotografija</th>
		<th>EMŠO</th>
		<th>Rojstvo</th>
		<th>Spol</th>
		<th>Profil</th>
		<th><a href="<?php echo $_SERVER['PHP_SELF']; ?>"><i class="icon-close"></i></a></th>
	</tr>
	<?php foreach ( $podatki as $i => $oseba ) : ?>

		<tr>
			<td><?php echo ++ $i; ?></td>
			<td><?php echo $oseba['ime']; ?></td>
			<td><?php echo $oseba['priimek']; ?></td>
			<td><?php echo $oseba['izobrazba']; ?></td>
			<td>
				<strong><?php echo $oseba['ime']; ?><?php echo $oseba['priimek']; ?></strong>
				<br>
				<?php echo $oseba['naslov']['ulica']; ?>
				<br>
				<?php echo $oseba['naslov']['posta']; ?>
				<?php echo $oseba['naslov']['mesto']; ?>
			</td>
			<td>
				<?php echo $oseba['kontakt']['telefon']; ?>
				<br>
				<?php echo $oseba['kontakt']['mobitel']; ?>
				<br>
				<a href="mailto:<?php echo $oseba['kontakt']['email']['email_naslov']; ?>" target="_blank">
					<?php echo $oseba['kontakt']['email']['email_naslov']; ?>
				</a>
			</td>
			<td>
				<img src="<?php echo $oseba['fotografija']['mala']; ?>" alt="">
			</td>
			<td><?php echo $oseba['emso']; ?></td>
			<td>
				<?php echo get_dan( date( 'N', strtotime( $oseba['rojstvo'] ) ), 'L' ); ?>,
				<?php echo date( 'j. n. Y', strtotime( $oseba['rojstvo'] ) ); ?>
				<br>
				Starost: <?php echo get_starost( $oseba['rojstvo'] ); ?>
			</td>
			<td><?php echo $oseba['spol']; ?></td>
			<td>
				<?php echo date( 'd. m. Y, G:i:s', strtotime( $oseba['ustvarjen'] ) ); ?>
				<br>
				<?php echo date( 'd. m. Y, G:i:s', strtotime( $oseba['posodobljen'] ) ); ?>
			</td>
			<td>
				<a href="?id=<?php echo $oseba['id']; ?>">
					<i class="icon-pencil"></i>
				</a>
			</td>
		</tr>

	<?php endforeach; ?>

</table>

</body>
</html>
